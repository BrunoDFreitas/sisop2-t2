// Solução com mutex e variáveis de condição

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define TAM		100

pthread_t	filosofos[TAM];
pthread_mutex_t	mesa_mutex[TAM];
pthread_cond_t	mesa_cond[TAM];
unsigned char	gdireita[TAM];
unsigned char	gesquerda[TAM];
unsigned char	garfos[TAM];
char   		estado[TAM];

pthread_mutex_t print_mutex;

typedef struct{
	int num_fil;
	int tot_fil;
} param;

// Imprime os estados dos filósofos (T, H ou E)
void print(int tam) {
        pthread_mutex_lock(&print_mutex);
		int i;
		for (i = 0; i < tam; i++)
			printf("%c ", estado[i]);
		printf("\n");
		pthread_mutex_unlock(&print_mutex);
}

// Função da thread
void *run(void *threadid) {
	param a = *(param *) threadid;
	int id = a.num_fil;
	int tam = a.tot_fil;
        int x = 1;

	while (1) {
		estado[id] = 'T'; //Thinking
		print(tam);
                x = 1 + ( rand() % 10 );
		usleep(x*1000000); //multiplica por 1000000 para transformar ms em s

		estado[id] = 'H';	//Hungry
		print(tam);
		pthread_mutex_lock(&mesa_mutex[id]);
		while (garfos[id] != 2) {	//Teste para pegar os garfos usando variáveis de condição

			pthread_cond_wait(&mesa_cond[id], &mesa_mutex[id]);
		}

		pthread_mutex_lock(&mesa_mutex[gesquerda[id]]); //mutex para pegar o garfo esquerdo
		garfos[gesquerda[id]]--;
		pthread_mutex_unlock(&mesa_mutex[gesquerda[id]]);
		pthread_mutex_lock(&mesa_mutex[gdireita[id]]);	//mutex para pegar o garfo direito
		garfos[gdireita[id]]--;
		pthread_mutex_unlock(&mesa_mutex[gdireita[id]]);
		pthread_mutex_unlock(&mesa_mutex[id]);

		estado[id] = 'E';	//eating
		print(tam);
                x = 1 + ( rand() % 10 );
		usleep(x*1000000);

		pthread_mutex_lock(&mesa_mutex[id]);
										//devolve os garfos a mesa
		pthread_mutex_lock(&mesa_mutex[gesquerda[id]]);	//mutex para devolver o garfo esquerdo
                garfos[gesquerda[id]]++;
		pthread_cond_signal(&mesa_cond[gesquerda[id]]);	//sinaliza a condição de garfo livre
                pthread_mutex_unlock(&mesa_mutex[gesquerda[id]]);

		pthread_mutex_lock(&mesa_mutex[gdireita[id]]);	//mutex para devolver o garfo direito
		garfos[gdireita[id]]++;
		pthread_cond_signal(&mesa_cond[gdireita[id]]);	//sinaliza a condição de garfo livre
		pthread_mutex_unlock(&mesa_mutex[gdireita[id]]);

		pthread_mutex_unlock(&mesa_mutex[id]);
	}
	pthread_exit(NULL);
}


int main(int argc, char **argv)
{
	int NUM_FILOSOFOS = atoi(argv[1]);
	param a[NUM_FILOSOFOS];
	// Define uma semente para o gerador de números aleatórios
	srand((unsigned int) time( NULL ));

     pthread_mutex_init(&print_mutex, NULL);

	// Inicializa as estruturas
	int id;
	for (id = 0; id < NUM_FILOSOFOS; id++) {
		pthread_mutex_init(&mesa_mutex[id], NULL);
		pthread_cond_init(&mesa_cond[id], NULL);
		gdireita[id] = (id + 1) % NUM_FILOSOFOS;
		gesquerda[id] = (id + NUM_FILOSOFOS - 1) % NUM_FILOSOFOS;
		garfos[id] = 2;
		estado[id] = 'T';
                a[id].num_fil = id;
		a[id].tot_fil = NUM_FILOSOFOS;
	}

	// Inicializa as pthreads
	for (id = 0; id < NUM_FILOSOFOS; id++) {


		pthread_create(&filosofos[id], NULL, run, (void*)&a[id]);
	}

	// Termina
	pthread_exit(NULL);
	return 0;
}
