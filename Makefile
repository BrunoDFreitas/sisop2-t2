############################# Makefile ##########################

# SEMAFOROS
SEM_EXE=TSemaforo
SEM_OBJ=$(SEM_EXE).o
SEM_SRC=$(SEM_EXE).c

# MUTEX
MUT_EXE=TMutex
MUT_OBJ=$(MUT_EXE).o
MUT_SRC=$(MUT_EXE).c


all: make_mutex make_semaforo


# Problema dos filósofos em semaforo
make_semaforo: 
	gcc -o $(SEM_OBJ) -c $(SEM_SRC)
	gcc -o $(SEM_EXE) $(SEM_OBJ) -lpthread


# Problema dos filósofos em mutex
make_mutex: 
	gcc -o $(MUT_OBJ) -c $(MUT_SRC)
	gcc -o $(MUT_EXE) $(MUT_OBJ) -lpthread


clean:
	rm -rf *.o
	rm -rf $(SEM_EXE)
	rm -rf $(MUT_EXE)

