//Solução com semaforos

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define TAM 100

typedef struct {
    int num_fil;
    int tot_fil;
} param;

int estado[TAM];
//semaforos
sem_t mutex;
sem_t sem_fil[TAM];
sem_t cond;

//funcao que mostra o estado dos N filosofos

void print(int tam) {
    int i;
    for (i = 0; i < tam; i++) {
        if (estado[i] == 0)
            printf("T ");
        if (estado[i] == 1)
            printf("H ");
        if (estado[i] == 2)
            printf("E ");
    }
    printf("\n");

}

//funcao que testa se o filosofo pode comer
void test(int i, int tot) {
    int esquerda = (i + tot - 1) % tot;
    int direita = (i + 1) % tot;
    if (estado[i] == 1 && estado[esquerda] != 2 && estado[direita] != 2) {
        estado[i] = 2; //2 = comendo
        print(tot);
        sem_post(&sem_fil[i]);
    }
}


//acao do filosofo
void *run(void *threadid) {
    param a = *(param*) threadid;
    int x;
    int id = a.num_fil;
    int tam = a.tot_fil;
    int esquerda = (id + tam - 1) % tam;
    int direita = (id + 1) % tam;
    while (1) {
        estado[id] = 0; //pensando = 0
        print(tam);
        x = 1 + (rand() % 10);
        usleep(x * 1000000); //multiplica por 1000000 para transformar ms em s
        //pegar garfos;
        estado[id] = 1; //1 = fome
        print(tam);
        sem_wait(&mutex);

        test(id, tam);
        sem_post(&mutex);
        sem_wait(&sem_fil[id]);
        x = 1 + (rand() % 10);
        usleep(x * 1000000); //multiplica por 1000000 para transformar ms em s
        //devolver garfos
        sem_wait(&mutex);
        estado[id] = 0;
        print(tam);
        test(esquerda, tam);
        test(direita, tam);
        sem_post(&mutex);

    }

}


int main(int argc, char **argv) {
    int NUM_FILOSOFOS = atoi(argv[1]);
    int res, id;
    int i;
    param a[NUM_FILOSOFOS];
    // Define uma semente para o gerador de números aleatórios
    srand((unsigned int) time(NULL));

    pthread_t filosofos[NUM_FILOSOFOS];

    void *thread_result;

    //inicia os semaforos e os estados

    res = sem_init(&mutex, 0, 1);
    if (res != 0) {
        perror("Erro na inicialização do semaforo!");
        exit(EXIT_FAILURE);
    }

    for (id = 0; id < NUM_FILOSOFOS; id++) {
        res = sem_init(&sem_fil[id], 0, 0);
        if (res != 0) {
            perror("Erro na inicialização do semaforo!");
            exit(EXIT_FAILURE);
        }

        estado[id] = 0;
        a[id].num_fil = id;
        a[id].tot_fil = NUM_FILOSOFOS;
    }

    //cria as threads filosofos

    for (i = 0; i < NUM_FILOSOFOS; i++) {
        res = pthread_create(&filosofos[i], NULL, run, &a[i]);
        if (res != 0) {
            perror("Erro na inicialização da thread!");
            exit(EXIT_FAILURE);
        }
    }

    //faz um join nas threads

    for (i = 0; i < NUM_FILOSOFOS; i++) {
        res = pthread_join(filosofos[i], &thread_result);
        if (res != 0) {
            perror("Erro ao fazer join nas threads!");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
